import 'whatwg-fetch';

/**
 * replaces one class with another
 *
 * @param el Target element
 * @param oldClass The class to remove
 * @param newClass The class to be added
 */
export function replaceClass(el, oldClass, newClass) {
  if (!el || !oldClass || !newClass) {
    console.warn('replaceClass() requires all arguments to be defined');
    return;
  }

  el.classList.remove(oldClass);
  el.classList.add(newClass);
}

/**
 * ms to sleep for. Use in async function like this:
 *    `await sleep(500)`
 */
export const sleep = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));


/**
 * Generic function used for fetch responses that only need to
 * have their json text parsed returned for next link in promise chain
 *
 * @param res
 */
export const genericFetchResponseHandler = async (res) => {
  // success with JSON body
  if(res.status === 200) {
    // TODO: need to standalize what error message object looks like
    return res.json();

  // success with no body
  } else if (res.status === 204) {
    return null;

  } else {
    // error body is a promise, need to await to get json from it
    const errBody = await res.json();
    throw new Error(errBody.Message || 'Unknown server error');
  }
}

const standardFetchOptions = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  method: 'POST',
  // credentials: 'include'
};

/**
 * @function submitForm
 * @param {Object} values
 * @param {String} url
 * @param {Object} opts options to overwrite standardFetchOptions
 * @returns {Promise<void>}
 */
export const submitForm = async (values, url, opts) => {
  const fetchOptions = {
    ...standardFetchOptions,
    ...opts,
    body: JSON.stringify(values)
  };

  const res = await window.fetch(url, fetchOptions);
  return await genericFetchResponseHandler(res);
};

