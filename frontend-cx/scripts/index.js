import Carousel from '../components/carousel';
import Forms from '../components/forms';

// this file will be extracted and generated as styles.css (setup in config/styles.config)
import '../styles/index.scss';

new Carousel('.carousel');
new Forms('.forms');