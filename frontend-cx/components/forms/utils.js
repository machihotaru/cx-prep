/**
 * Expects button in template to look like:
 *
 * <a class="button submit" title="Submit" role="button"><span>Submit</span></a>
 *
 * @param submitBtn
 * @param enabled
 */
export const setSubmitEnabled = (submitBtn, enabled = false) => {
  if (enabled) {
    submitBtn.removeAttribute('disabled');

  } else {
    submitBtn.setAttribute('disabled', '');
  }
};

export const setSubmitting = (submitBtn, submitting = false) => {
  if (submitting) {
    submitBtn.classList.add('is-loading')

  } else {
    submitBtn.classList.remove('is-loading')
  }
};

/**
 * State example:
 *
 * let formFieldDirtyState = {
 *    name: false,
 *    email: false,
 *    enquiryType: false,
 *    message: false,
 *  };
 *
 * @param state
 * @param fieldName string
 * @return state
 */
export const markFieldAsDirty = (state, fieldName) => {
  if (!Object.keys(state).includes(fieldName)) throw new Error(`Unknown field, '${fieldName}'`)

  return {...state, [fieldName]: true};
};

/**
 * State example:
 *
 * let formFieldDirtyState = {
 *    name: false,
 *    email: false,
 *    enquiryType: false,
 *    message: false,
 *  };
 *
 * @param state
 * @return state
 */
export const resetDirtyFields = state => {
  return Object.keys(state)
    .reduce((dirtyFields, key) => ({...dirtyFields, [key]: false}), {})
};
