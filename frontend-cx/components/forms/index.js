import serialize from 'form-serialize';
import { replaceClass, submitForm } from '../../scripts/utils';

/**
 * @export
 * @class Forms
 * @params el, opts
 */
class Forms {
  constructor(el, opts = {}) {
    this.el = (typeof el === 'string') ? document.querySelectorAll(el):[el];
    if(!this.el) { return; }

    this.opts = opts;
    if(!this.init) {
      this.initListeners = this.initListeners.bind(this);
      this.initialize();
    }
  }

  /**
   * 
   * @param {Number} idx 
   * call `submitForm` api function (Promise)
   * set `success` or `invalid` status to the form
   * by adding CSS class to the parent <form />
   */
  async submitForm(idx) {
    const { el, values, submitButton } = this.form[idx];
    const apiURL = el.getAttribute('action');
    if(!apiURL) { return; } // TODO: do something?!

    const showSuccess = () => {
      const { el } = this.form[idx];
      el.classList.add('forms--success');
    }
  
    const showError = err => {
      const { el, errorBox } = this.form[idx];
      errorBox.innerHTML = err;
      el.classList.add('forms--invalid');
    }
   
    el.classList.add('is-loading');
    submitButton.setAttribute('disabled', true);

    try {
      const res = await submitForm(values, apiURL);

      // TODO: find how error response in 200 comes along
      if(res && res.error) {
        showError(res.error);
      } else {
        showSuccess();
      }

    } catch (err) {
      showError(err);
    } finally {
      el.classList.remove('is-loading');
      submitButton.removeAttribute('disabled');
    }
  }

  /**
   * @param {Number} idx
   * using native validity: required or field type expected
   * any custom validations need tweaks
   */
  validateForm(idx) {
    const values = serialize(this.form[idx].el, { hash: true });
    let formValid = true;
    let formDirty = false;

    const validateField = input => new Promise((resolve) => {
      let valid = true;
      let dirty = false;
      if(input.classList.contains('is-dirty')) {
        dirty = true;
        formDirty = true;
      }
      // validity must be compatible from IE10
      if(!input.validity.valid) {
        valid = false;
        formValid = false;
      }
      input.classList[valid || !dirty ? 'remove' : 'add']('is-invalid');
      resolve(valid);
    });

    const res = Promise.all(this.form[idx].fields.map(validateField));
    const action = formValid && formDirty ? 'remove' : 'set';
    this.form[idx].submitButton[`${action}Attribute`]('disabled', true);
    // use values for submission action
    this.form[idx] = { ...this.form[idx], values, formValid, formDirty };
    
    return res;
  }

  /**
   * @param {Element} form 
   * @param {Number} idx 
   * make object for each forms like:
   * { el, errors, values }
   */
  initListeners(form, idx) {
    this.form[idx] = { 
      el: form,
      errorBox: form.querySelector('.alert-danger')
    };

    const submitButton = form.querySelector('[type="submit"]');
    let fields = form.querySelectorAll('input', 'select', 'textarea');
    // because nodelist is not pure array, convert it to Array to do amazing things!
    fields = Array.prototype.slice.call(fields);
    fields.forEach(input => {
      const type = input.getAttribute('type');
      const action = ['radio', 'checkbox'].includes(type) ? 'change' : 'blur';

      input.addEventListener(action, () => {
        replaceClass(input, 'is-invalid', 'is-dirty')
        form.classList.remove('forms--invalid', 'forms--success');

        if(type === 'checkbox') {
          input.value = input.value === 'on';
        }
        this.validateForm(idx);
      });
    });
    
    form.addEventListener('submit', e => {
      e.preventDefault();
      this.validateForm(idx).then(() => {
        const { formValid, formDirty  } = this.form[idx];
        // now this.form[idx] must be updated
        if(formValid && formDirty) {
          this.submitForm(idx);
        }
      });
    });

    this.form[idx] = { ...this.form[idx], fields, submitButton };
    this.validateForm(idx);
  }

  initialize() {
    this.init = true;
    this.form = [];
    [].forEach.call(this.el, this.initListeners);
  }
}

export default Forms;