import 'slick-carousel';
import debounce from 'debounce';

/**
 * @export
 * @class Carousel
 * @param {Object} of el, opts
 * @param {Element} el
 * @param {Object} opts - slick options overwrite
 */
class Carousel {
  constructor(el, opts = {}) {
    this.el = (typeof el === 'string') ? document.querySelectorAll(el):[el];
    if(!this.el) { return; }

    this.current = 0;

    // slick options (can overwrite from parent)
    this.opts = {
      infinite: true,
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: 'ease-in-out',
      fade: false,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ],
      ...opts,
    };

    if(!this.init) {
      this.initialize();
    }
  }

  /**
   * @function attachCarousel
   * initial settings for each gallery into slick carousel
   * modal setup for sync flagged gallery
   */
  attachCarousel() {
    const initCarousel = (target, idx) => {
      const carousel = $(target);
      if(!carousel || carousel.hasClass('slick-initialized')) { return; }

      // extra option overwrite
      let opts = this.opts;
      let custom = carousel.data('slick');
      if(custom && typeof custom === 'object') {
        opts = { ...opts, custom };
      }

      let slider = null;
      const alignHeight = slick => {
        slider = slick ? slick.$slider : slider;
        if(opts.adaptiveHeight || !slider) { return; }
        
        const slides = slider.find('.slick-slide'); // NOT slick.$slides, doesn't get cloned items

        // release each slide items height once
        slides.each((i, slide) => $(slide).height('auto'));
        // add fake interval to wait
        setTimeout(() => {
          // then get the natural height
          const height = slider.height();
          // set the natural height back to each slide items again
          slides.each((i, slide) => $(slide).height(height));
        }, 0);

      };

      carousel
      .on('init', (e, slick) => {
        alignHeight(slick);
      })
      // .on('beforeChange', (e, slick, currentSlide, nextSlide) => {
      // })
      // .on('afterChange', (e, slick, currentSlide) => {  
      // });
      carousel.slick(opts);
      window.onresize = () => debounce(alignHeight(), 1000);
    };
    [].forEach.call(this.el, initCarousel);
  }

  initialize() {
    this.init = true;
    this.attachCarousel();
  }
}

export default Carousel;
