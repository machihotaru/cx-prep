const fs = require('fs-extended');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const REGEX_HBS = /\.(hbs|html)$/;

const pagesDirs = path.join(__dirname, '../pages/');
const helperDirs = path.join(__dirname, './handlebars/');
const partialDirs = path.join(__dirname, '../components/');

// Read all files in src/pages folder
// Pick only *.hbs files
const pages = fs.listAllSync(pagesDirs, { recursive: 1 }).filter(fileName => REGEX_HBS.test(fileName));

module.exports = isDev => {
  // directory under PUBLIC_PATH
  const htmlDir = isDev ? '' : 'html/';
  // TODO: just dummy api URL
  const apiURL = isDev ? 'https://agi-website-dev-deploy.azurewebsites.net' : ''; // relative path on prod, unlike in dev

  // Create custom webpack-plugin for every available page to render it as *.html
  const pagesPlugins = pages.map(fileName => {
    const template = path.join(pagesDirs, fileName); // pages/index.hbs
    const html = fileName.replace(REGEX_HBS, '.html'); // index.hbs -> index.html
    const filename = `${htmlDir}${html}`; // html/index.html
    const REGEX_EMAIL = /^z_emails/;

    return new HtmlWebpackPlugin({
      filename,
      template,
      // variables globally injected
      templateParameters: {
        apiURL,
        filename,
        pages: pages.filter(page => page !== 'index.hbs').map(page => page.replace(REGEX_HBS, '.html')),
      },
      inject: !REGEX_EMAIL.test(fileName),
      xhtml: true,
    })
  });

  return {
    loaders: [
      // Compile handlebars template
      {
        test: /\.(hbs|html)$/,
        loader: 'handlebars-loader', 
        options: {
          helperDirs,
          partialDirs
        } 
      }
    ],
  
    // on production build, clean the old one then extract
    plugins: (isDev ? [] : [
      new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: [`${htmlDir}*`] })
    ]).concat(pagesPlugins)
  };
};