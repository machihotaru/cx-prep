module.exports = {
  fontName: 'icon',
  template: './components/icons/_icons.scss.njk',
  templateFontPath: '/static-cx/assets/icons/',
  templateClassName: 'icons',
  dest: '../static-cx/assets/icons/',
  destTemplate: './components/icons/',
  normalize: true,
  bail: true,
  height: 100,
  fontHeight: 100, // needs to be high so that we get reasonable sharpness.
  prependUnicode: true
};
