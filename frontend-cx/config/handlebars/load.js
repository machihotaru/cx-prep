/**
 * @function load (load specific json data to use)
 * @param target this
 * @param filename eg 'global'
 * eg {{load this 'global'}}
 */
module.exports = (target, filename) => {
  Object.assign(target, require(`../../data/${filename}.json`) || {});
};