/**
 * @function eq (compare a and b)
 * @param a compare value 1
 * @param b compare value 2
 * @param options injected by handlebars
 * eg {{#eq active "about"}} active{{/eq}}
 */
module.exports = (a, b, options) => {
  if(a === b) { return options.fn(this); }
  return options.inverse(this);
};