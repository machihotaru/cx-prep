module.exports = {
  loaders: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader'
      }
    },
    {
      test: /\.(svg|eot|ttf|woff|woff2)?$/,
      loader: 'file-loader',
      options: {
          name: './static-cx/assets/fonts/[name].[ext]'
      }
    },
    {
      test: /\.(png|svg|jpg|gif)$/,
      use: [
        'file-loader'
      ]
    }
  ]
};