const path = require('path');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const filename = 'assets/styles.css';

module.exports = isDev => ({
  loaders: {
    test: /\.s?[ac]ss$/,
    use: [
      isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
      'css-loader',
      {
        loader: 'postcss-loader',
        options: {
          plugins: () => [
            require('postcss-flexbugs-fixes'),
            autoprefixer({
              flexbox: 'no-2009',
            }),
          ],
        }
      },
      {
        loader: 'sass-loader',
        options: {
          sassOptions: {
            includePaths: [
              path.resolve(__dirname, '../'),
              path.resolve(__dirname, '../node_modules')
            ]
          },
          prependData: `$fontPath: '/static-cx/assets/fonts/';`,
        }
      }
    ]
  },

  // on production build, clean the old one then extract
  plugins: isDev ? [] : [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: [filename] }),
    new MiniCssExtractPlugin({ filename })
  ]
});