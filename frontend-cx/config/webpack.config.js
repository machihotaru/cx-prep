const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (env, argv) => {
  const isDev = argv.mode === 'development';
  const PUBLIC_PATH = '../static-cx';
  const resources = require('./resources.config');
  const pages = require('./pages.config')(isDev);
  const styles = require('./styles.config')(isDev);
  console.log('test', path.resolve(path.join(PUBLIC_PATH)))
  return {
    entry: {
      app: [
        './scripts/polyfills.js',
        './scripts/index.js'
      ]
    },

    devtool: isDev ? 'source-map' : false,

    externals: { jquery: 'jQuery' },

    output: {
      filename: 'assets/[name].bundle.js',
      path: path.resolve(path.join(PUBLIC_PATH))
    },

    devServer: isDev ? { contentBase: path.join(__dirname, '../../') } : {},

    module: {
      rules: (resources.loaders || [])
      .concat(pages.loaders)
      .concat(styles.loaders)
    },

    plugins: (isDev ? [] : [
      new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['assets/*.js'] })
    ])
    .concat(pages.plugins)
    .concat(styles.plugins)
    /*
      uncomment this and its require to get a detailed
      report of what's in the bundles
    */
    // .concat([new BundleAnalyzerPlugin()])
  };
};